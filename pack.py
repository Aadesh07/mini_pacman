import pygame
from pygame.locals import *
pygame.init()

SCREENSIZE = (600, 400)
screen = pygame.display.set_mode(SCREENSIZE, 0, 32)
clock = pygame.time.Clock()
x, y = (300, 200)
while True:
    time_passed = clock.tick(30) / 1000.0
    key_pressed = pygame.key.get_pressed()
    background = pygame.surface.Surface(SCREENSIZE).convert()
    background.fill((0, 0, 0))
    for event in pygame.event.get():
        if event.type == QUIT:
            exit()

    if key_pressed[K_UP]:
        y -= 3
    elif key_pressed[K_DOWN]:
        y += 3
    elif key_pressed[K_LEFT]:
        x -= 3
    elif key_pressed[K_RIGHT]:
        x += 3

    screen.blit(background, (0, 0))
    pygame.draw.circle(screen, (255,255,0), (x,y), 16)
    pygame.display.update()